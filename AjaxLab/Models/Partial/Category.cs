﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AjaxLab.Models
{
    [MetadataType(typeof(CategoryMetaData))]
    public partial class Category { }

    public class CategoryMetaData
    {

        public object CategoryID { get; set; }
    }

    [MetadataType(typeof(SupplierMetaData))]
    public partial class Supplier { }

    public class SupplierMetaData
    {

 
        public object SupplierID { get; set; }
    }
}