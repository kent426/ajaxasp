﻿using AjaxLab.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AjaxLab.Controllers
{
    public class HomeController : Controller
    {
        private NorthwindEntities ctx = new NorthwindEntities();

        public ActionResult Index()
        {

            NorthwindEntities ctx = new NorthwindEntities();

            var cC = ctx.Categories.OrderBy(c => c.CategoryID).ToList();
            cC.Insert(0, new Category { CategoryID = 0 });

            var cdrop = new SelectList(cC
               ,
              "CategoryID", "CategoryName");


            ViewBag.Categories = cdrop;

            var sC = ctx.Suppliers.OrderBy(c => c.SupplierID).ToList();
            sC.Insert(0, new Supplier { SupplierID = 0 });



            ViewBag.Suppliers = new SelectList(
               sC,
              "SupplierID", "CompanyName");

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}