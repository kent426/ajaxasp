﻿using AjaxLab.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AjaxLab.Controllers
{
    public class ProductController : Controller
    {
        NorthwindEntities ctx = new NorthwindEntities();

        // GET: Product
        public ActionResult Index()
        {
            return View();
        }



        public PartialViewResult ProductSearch(int Categories, int Suppliers)
        {

            IEnumerable<Product> pros;
            var s = "";
            if (Categories == 0 && Suppliers == 0)
            {
                pros  = ctx.Products.ToList();

                s = "display all products";


            } else if(Categories == 0)
            {
                pros = ctx.Products.Where(p=> p.SupplierID == Suppliers).ToList();
                s = "display products that belong to the " + ctx.Suppliers.Find(Suppliers).CompanyName +"supplier.";


            } else if(Suppliers == 0)
            {
                pros = ctx.Products.Where(p => p.CategoryID == Categories).ToList();
                s = "display products that belong to the " + ctx.Categories.Find(Categories).CategoryName + "category.";
            }
            else
            {
                pros = ctx.Products
                .Where(p => p.CategoryID == Categories)
                .Where(p => p.SupplierID == Suppliers)
                .ToList();
                s = "display products that belong to " + ctx.Categories.Find(Categories).CategoryName 
                    + " category from "
                    + ctx.Suppliers.Find(Suppliers).CompanyName
                    + " supplier";
            }


            List<Object> o = new List<Object>() { pros, s };
            return PartialView("_ProductPartial", o);

            //IEnumerable<Product> pros = ctx.Products
            //    .Include("Category")
            //    .Include("Supplier").Where(p =>p.CategoryID == Categories)
            //    .Where(p => p.SupplierID== Suppliers)
            //    .ToList();

            //return PartialView("_ProductPartial", pros);
            

        }
    }
}